<?php
declare(strict_types=1);

require_once './vendor/autoload.php';

use DI\ContainerBuilder;
use App\Interfaces\Command\ApplicationCommandInterface;

$shortOpts = 'a:f:';
$longOpts  = [
    'action:',
    'file:'
];

$options = getopt($shortOpts, $longOpts);

$builder = new ContainerBuilder();
$builder->addDefinitions('config.php');
$container = $builder->build();

$container->get(ApplicationCommandInterface::class)->startApplication($options);

