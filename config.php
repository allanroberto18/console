<?php
declare(strict_types=1);

use App\Interfaces\Handler\CommandHandlerInterface;
use App\Handler\CommandHandler;

use App\Interfaces\Command\ApplicationCommandInterface;
use App\Command\ApplicationCommand;

use App\Helpers\FileManagerHelper;

use App\Interfaces\Helpers\FileManagerHelperInterface;
use App\Interfaces\Services\SumServiceInterface;
use App\Interfaces\Services\MinusServiceInterface;
use App\Interfaces\Services\DivisionServiceInterface;
use App\Interfaces\Services\MultiplyServiceInterface;

use App\Services\SumService;
use App\Services\MinusService;
use App\Services\DivisionService;
use App\Services\MultiplyService;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Formatter\LineFormatter;
use \Psr\Log\LoggerInterface;

return [
    Psr\Log\LoggerInterface::class => DI\factory(function() {
        $logger = new Logger('mylog');

        $fileHandler = new StreamHandler('./var/logs.log', Logger::DEBUG);
        $fileHandler->setFormatter(new LineFormatter());
        $logger->pushHandler($fileHandler);

        return $logger;
    }),
    FileManagerHelperInterface::class => DI\create(FileManagerHelper::class),
    SumServiceInterface::class => function (FileManagerHelperInterface $fileManagerHelper, LoggerInterface $logger) {
        return new SumService($fileManagerHelper, $logger);
    },
    MinusServiceInterface::class => function (FileManagerHelperInterface $fileManagerHelper, LoggerInterface $logger) {
        return new MinusService($fileManagerHelper, $logger);
    },
    MultiplyServiceInterface::class => function (FileManagerHelperInterface $fileManagerHelper, LoggerInterface $logger) {
        return new MultiplyService($fileManagerHelper, $logger);
    },
    DivisionServiceInterface::class => function (FileManagerHelperInterface $fileManagerHelper, LoggerInterface $logger) {
        return new DivisionService($fileManagerHelper, $logger);
    },
    CommandHandlerInterface::class => function() {
        return new CommandHandler();
    },
    ApplicationCommandInterface::class => function (
        CommandHandlerInterface $handler,
        SumServiceInterface $sumService,
        MinusServiceInterface $minus,
        MultiplyServiceInterface $multiplyService,
        DivisionServiceInterface $divisionService,
        LoggerInterface $logger
    ) {
        return new ApplicationCommand($handler, $sumService, $minus, $multiplyService, $divisionService, $logger);
    }
];
