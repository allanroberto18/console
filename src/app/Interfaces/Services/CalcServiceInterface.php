<?php
declare(strict_types=1);

namespace App\Interfaces\Services;

interface CalcServiceInterface
{
    /**
     * @param string $file
     * @return void
     */
    public function saveResults(string $file): void;
}
