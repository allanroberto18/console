<?php
declare(strict_types=1);

namespace App\Interfaces\Command;

interface ApplicationCommandInterface
{
    /**
     * @param array $options
     * @return void
     */
    public function startApplication(array $options): void;
}
