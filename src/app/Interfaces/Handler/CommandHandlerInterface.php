<?php
declare(strict_types=1);

namespace App\Interfaces\Handler;

interface CommandHandlerInterface
{
    /**
     * @param array $options
     * @return void
     */
    public function processOptions(array $options): self;

    /**
     * @return string|null
     */
    public function getAction(): ?string;

    /**
     * @return string|null
     */
    public function getFile(): ?string;
}
