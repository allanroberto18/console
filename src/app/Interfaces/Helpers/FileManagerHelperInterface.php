<?php
declare(strict_types=1);

namespace App\Interfaces\Helpers;

interface FileManagerHelperInterface
{
    /**
     * @param string $file
     * @return array
     */
    public function getContentFromFile(string $file): array;

    /**
     * @param string $file
     * @param string $line
     * @param string $mode
     * @return void
     */
    public function writeLine(string $file, string $line, string $mode): void;
}
