<?php
declare(strict_types=1);

namespace App\Handler;

use App\Interfaces\Handler\CommandHandlerInterface;

class CommandHandler implements CommandHandlerInterface
{
    /** @var string|null $action */
    private $action;

    /** @var string|null $file */
    private $file;

    /**
     * @param array $options
     * @return CommandHandlerInterface
     */
    public function processOptions(array $options): CommandHandlerInterface
    {
        $keys = array_keys($options);
        foreach ($keys as $key) {
            $this->setAction($options, $key);
            $this->setFile($options, $key);
        }

        return $this;
    }

    /**
     * @param array $options
     * @param string $key
     * @return void
     */
    private function setAction(array $options, string $key): void
    {
        if ($key === 'a' || $key === 'action') {
            $this->action = $options[$key];
        }
    }

    /**
     * @param array $options
     * @param string $key
     * @return void
     */
    private function setFile(array $options, string $key): void
    {
        if ($key === 'f' || $key === 'file') {
            $this->file = $options[$key];
        }
    }

    /**
     * @return string|null
     */
    public function getAction(): ?string
    {
        return $this->action;
    }

    /**
     * @return string|null
     */
    public function getFile(): ?string
    {
        return $this->file;
    }
}
