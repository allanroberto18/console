<?php
declare(strict_types=1);

namespace App\Command;

use App\Interfaces\Command\ApplicationCommandInterface;
use App\Interfaces\Services\SumServiceInterface;
use App\Interfaces\Services\MinusServiceInterface;
use App\Interfaces\Services\MultiplyServiceInterface;
use App\Interfaces\Services\DivisionServiceInterface;
use App\Interfaces\Handler\CommandHandlerInterface;
use Psr\Log\LoggerInterface;

class ApplicationCommand implements ApplicationCommandInterface
{
    /** @var SumServiceInterface $sumService */
    private $sumService;

    /** @var MinusServiceInterface $minusService */
    private $minusService;

    /** @var MultiplyServiceInterface $multiplyService */
    private $multiplyService;

    /** @var DivisionServiceInterface $divisionService */
    private $divisionService;

    /** @var LoggerInterface $loggerInterface */
    private $loggerInterface;

    /** @var CommandHandlerInterface $handler */
    private $handler;

    /**
     * RunApplicationCommand constructor.
     * @param CommandHandlerInterface $handler
     * @param SumServiceInterface $sumService
     * @param MinusServiceInterface $minusService
     * @param MultiplyServiceInterface $multiplyService
     * @param DivisionServiceInterface $divisionService
     * @param LoggerInterface $loggerInterface
     */
    public function __construct(
        CommandHandlerInterface $handler,
        SumServiceInterface $sumService,
        MinusServiceInterface $minusService,
        MultiplyServiceInterface $multiplyService,
        DivisionServiceInterface $divisionService,
        LoggerInterface $loggerInterface
    ) {
        $this->handler = $handler;
        $this->sumService = $sumService;
        $this->minusService = $minusService;
        $this->multiplyService = $multiplyService;
        $this->divisionService = $divisionService;
        $this->loggerInterface = $loggerInterface;
    }

    /**
     * @param array $options
     * @return void
     */
    public function startApplication(array $options): void
    {
        try {
            $handler = $this->handler->processOptions($options);
            switch ($handler->getAction()) {
                case 'plus':
                    $this->sumService->saveResults($handler->getFile());
                    break;

                case 'minus':
                    $this->minusService->saveResults($handler->getFile());
                    break;

                case 'multiply':
                    $this->multiplyService->saveResults($handler->getFile());
                    break;

                case 'division':
                    $this->divisionService->saveResults($handler->getFile());
                    break;
            }
        } catch (\Exception $ex) {
            $this->loggerInterface->error($ex->getMessage(), [ 'line' => $ex->getLine(), 'file' => $ex->getFile()]);
        }
    }
}
