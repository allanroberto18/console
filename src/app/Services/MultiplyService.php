<?php
declare(strict_types=1);

namespace App\Services;

use App\Interfaces\Services\MultiplyServiceInterface;

class MultiplyService extends BaseCalcService implements MultiplyServiceInterface
{
    /**
     * @param integer $firstValue
     * @param integer $secondValue
     * @return boolean
     */
    protected function isGood(int $firstValue, int $secondValue): bool
    {
        if ($this->calculate($firstValue, $secondValue) < 0) {
            return false;
        }

        return true;
    }

    /**
     * @param integer $firstValue
     * @param integer $secondValue
     * @return float
     */
    protected function calculate(int $firstValue, int $secondValue): float
    {
        return $firstValue * $secondValue;
    }
}
