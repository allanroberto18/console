<?php
declare(strict_types=1);

namespace App\Services;

use App\Helpers\FileManagerHelper;
use App\Interfaces\Helpers\FileManagerHelperInterface;
use App\Interfaces\Services\CalcServiceInterface;
use Psr\Log\LoggerInterface;

abstract class BaseCalcService implements CalcServiceInterface
{
    /**
     * @var string
     */
    const RESULT_FILE_NAME = 'result.csv';

    /**
     * @var FileManagerHelperInterface $fileManagerHelper
     */
    protected $fileManagerHelper;

    /**
     * @var LoggerInterface $loggerInterface
     */
    protected $loggerInterface;

    /**
     * SumService constructor.
     * @param FileManagerHelperInterface $fileManagerHelper
     * @param LoggerInterface $loggerInterface
     */
    public function __construct(FileManagerHelperInterface $fileManagerHelper, LoggerInterface $loggerInterface)
    {
        if(file_exists(self::RESULT_FILE_NAME)) {
            unlink(self::RESULT_FILE_NAME);
        }

        $this->fileManagerHelper = $fileManagerHelper;
        $this->loggerInterface = $loggerInterface;
    }

    /**
     * @param string $file
     * @return void
     */
    public function saveResults(string $file): void
    {
        $this->start();
        $data = $this->fileManagerHelper->getContentFromFile($file);
        foreach ($data as $datum) {
            $this->printLine($datum);
        }

        $this->stop();
    }

    /**
     * @param integer $firstValue
     * @param integer $secondValue
     * @return boolean
     */
    abstract protected function isGood(int $firstValue, int $secondValue): bool;

    /**
     * @param integer $firstValue
     * @param integer $secondValue
     * @return float
     */
    abstract protected function calculate(int $firstValue, int $secondValue): float;

    /**
     * @param integer $firstValue
     * @param integer $secondValue
     * @return void
     */
    private function wrongNumbers(int $firstValue, int $secondValue): void
    {
        $line = sprintf('numbers %d and %d are wrong', $firstValue, $secondValue).PHP_EOL;
        $this->loggerInterface->warning($line);
    }

    /**
     * @return void
     */
    private function start(): void
    {
        $line = 'Started plus operation'.PHP_EOL;
        $this->loggerInterface->info($line);
    }

    /**
     * @return void
     */
    private function stop(): void
    {
        $line = 'Finished plus operation'.PHP_EOL;
        $this->loggerInterface->info($line);
    }

    /**
     * @param integer $firstValue
     * @param integer $secondValue
     * @return void
     */
    private function result(int $firstValue, int $secondValue): void
    {
        $result = $this->calculate($firstValue, $secondValue);

        $line = sprintf('%d;%d;%d', $firstValue, $secondValue, $result).PHP_EOL;
        $this->fileManagerHelper->writeLine('result.csv', $line, FileManagerHelper::INSERTING_LINE);
    }

    /**
     * @param array $data
     * @return void
     */
    private function printLine(array $data): void
    {
        $firstValue = intval($data[0]);
        $secondValue = intval($data[1]);

        if ($this->isGood($firstValue, $secondValue) === true) {
            $this->result($firstValue, $secondValue);
            return;
        }

        $this->wrongNumbers($firstValue, $secondValue);
    }
}
