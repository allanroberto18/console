<?php
declare(strict_types=1);

namespace App\Services;

use App\Interfaces\Services\SumServiceInterface;

class SumService extends BaseCalcService implements SumServiceInterface
{
    /**
     * @param integer $firstValue
     * @param integer $secondValue
     * @return boolean
     */
    protected function isGood(int $firstValue, int $secondValue): bool
    {
        if ($firstValue < 0 && $secondValue < 0) {
            return false;
        }

        if ($firstValue < 0 && (abs($firstValue) > $secondValue)) {
            return false;
        }

        if ($secondValue < 0 && (abs($secondValue) > $firstValue)) {
            return false;
        }

        return true;
    }

    /**
     * @param integer $firstValue
     * @param integer $secondValue
     * @return float
     */
    protected function calculate(int $firstValue, int $secondValue): float
    {
        return $firstValue + $secondValue;
    }
}
