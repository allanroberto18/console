<?php
declare(strict_types=1);

namespace App\Helpers;

use App\Interfaces\Helpers\FileManagerHelperInterface;

class FileManagerHelper implements FileManagerHelperInterface
{
    const READING_FILE = 'r';
    const STARTING_FILE = 'x+';
    const INSERTING_LINE = 'a+';

    /**
     * @param string $file
     * @return array
     */
    public function getContentFromFile(string $file): array
    {
        $fp = fopen($file, self::READING_FILE);
        $result = [];
        while(($data = fgetcsv($fp, 1000, ';')) !== false) {
            $result[] = $data;
        }

        fclose($fp);

        return $result;
    }

    /**
     * @param string $file
     * @param string $line
     * @param string $mode
     * @return void
     */
    public function writeLine(string $file, string $line, string $mode): void
    {
        if (file_exists($file) === false) {
            $mode = self::STARTING_FILE;
        }

        $fp = fopen($file, $mode);
        fwrite($fp, $line);
        fclose($fp);
    }
}
