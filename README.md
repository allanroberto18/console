# About code

This test was made with native php 7.2

This test was developed consider principles like SOLID and DRY. 

Was not necessary use framework to adjust but two libraries  were included:

* [php-di](http://php-di.org/) - to use Dependency Injection;
* [monolog](https://github.com/Seldaek/monolog) - to replace way of register logs on this small application;

### Options Available

* plus - to count summ of the numbers on each row in the {file}
* minus - to count difference between first number in the row and second
* multiply - to multiply the numbers on each row in the {file} 
* division - to divide  first number in the row and second

`php console.php --action {action}  --file {file}`

#### Next step

Because the short time de refactor the code was node config and introduced TDD

